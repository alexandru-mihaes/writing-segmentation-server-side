# Artificial Intelligence - writing segmentation - server-side

## Requirements - server-side

1. Install [JDK & JRE](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04)
2. Install [Maven](https://www.mkyong.com/maven/how-to-install-maven-in-ubuntu/)

## Build and run

`cd writing-segmentation-server-side`

`mvn package && java -jar target/wrseg-1.0-SNAPSHOT.jar`

## API Documentation

Server is running on PORT 8082.

**POST** /upload-file

The following parameters are supported on this end-point:

* `file` - this parameter is REQUIRED

* `threshold` (`float`)

* `noise` (`int`)
* `usegauss` (`boolean`) - supported values: true, false, 0, 1

* `maxcolseps` (`int`)

* `maxseps` (`int`)

* `minscale` (`float`)

* `maxlines` (`int`)

*If some parameters are not transmitted, they will have null value on the AI Layer.*

**Exceptions:**

* `InvalidFileExtensionException` (throwed when file doesn't have an extension):

"status": 400,

"error": "Bad Request",

"message": "Invalid file extension"

* `UploadException` (throwed when the file cannot be saved on the file system):

"status": 400,

"error": "Bad Request",

"message": "Error in uploading image"

* `ProcessingException` (throwed when the algorithm doesn't provide a response):

"status": 400,

"error": "Bad Request",

"message": "Processing error"